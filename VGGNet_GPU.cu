
#include <stdio.h>
#include <cuda_runtime.h>
#include "layers.h"

//#define DEMO
#define DEBUG_PRINT
//#define DEBUG_PRINT_2
#define ELEMENT 0
//#define DEBUG_MAXPOOL
#define ELEMENTM 0

using namespace std;


/*
	Fully connected layers
	For input 7x7x512
	Mask size is also 7x7x512 for each output
	so if the output size is 4096 the number of weights will be 7*7*512*4096
	and output dimension will be 1x1x4096
*/
__global__ void fclayer(float *output, unsigned int op_width,unsigned int op_height, unsigned int op_depth, float *input,unsigned int in_width,unsigned int in_height,unsigned int in_depth,float *mask,unsigned int filters,float *bias)
{

	unsigned int dest =  blockIdx.x * blockDim.x + threadIdx.x ;
	
	if(dest < filters)
	{
		float product = 0.0f;
		unsigned int start_ind = 0;
		unsigned int mask_ind = dest * in_width * in_height/* *in_depth*/ ;
		unsigned int mask_range = mask_ind + (in_width * in_height /**in_depth*/) ; 
		
		while(mask_ind<mask_range)
		{
			product+= (input[start_ind] * mask[mask_ind]);
			start_ind++;
			mask_ind++;
		}			
		product+=bias[dest];
		
		//RELU
		if(product < 0.0f)
			product = 0.0;
		
		output[dest]=product;
	}	
}


/*
Pooling

Output dimensions = ((input volume size - filter size)/stride) + 1
for eg 
input size = 224x224x64
filer size = 2x2x64
stride = 2

output dimensions = ((224 - 2)/ 2) + 1 = 112x112x64
Depth is preserved
Just downsample input
*/

__global__ void maxpool(float *output,unsigned int op_width,unsigned int op_height,float *input,unsigned int width,unsigned int height,unsigned int filters,unsigned int stride,unsigned int kernel_width,unsigned int kernel_height)
{
	unsigned int dest =  blockIdx.x * blockDim.x + threadIdx.x ;
	
	//only support even sized matrices
	
	if(dest < op_width * op_height)
	{
		//printf("dest= %d\n",dest);
		float maxval = 0.0;
		unsigned int start_ind = (dest * stride) + ((dest/op_width) * width);
		unsigned int range = (((start_ind/width) + kernel_height -1 ) * width) + ((start_ind%width) + kernel_width) ; //check here
		while(start_ind < range)
		{
			unsigned int rowrange = start_ind + kernel_width;
#ifdef DEBUG_MAXPOOL
			if(dest == ELEMENTM)
			{
				printf("Element =%d start_ind=%u range =%u row range=%u width=%u\n",ELEMENTM,start_ind,range,rowrange,width);
			}
#endif
			
			while(start_ind < rowrange)
			{
				if(maxval < input[start_ind])
				{
					maxval = input[start_ind];
				}
				start_ind++;
#ifdef DEBUG_MAXPOOL
			if(dest == ELEMENTM)
			{
				printf("Element =%d value=%0.2f\n",ELEMENTM,input[start_ind]);
			}
#endif

			}
			start_ind = (start_ind - kernel_width) + width ;
		}	
		
		output[dest]= maxval;
#ifdef DEBUG_MAXPOOL
			if(dest == ELEMENTM)
			{
				printf("Element =%d maxval = %0.2f\n",ELEMENTM,output[dest]);
			}
#endif
		
	}
	
}

/*
Convolution

Output dimensions = ((input volume size - filter size  + 2 * zero padding) / stride) + 1
for eg
input size = 5x5xD
filer size = 3x3xD
zero padding = 1
stride = 1

output dimensions = ((5 - 3 + 2*1 )/ 1) + 1 = 5x5xD
D is depth
filters should have same depth as input

*/

__global__ void conv1d(float *output,float *input,unsigned int width,unsigned int height,float *mask,unsigned int mask_width,unsigned int mask_height,unsigned int filters,unsigned int op_filters,float *bias)
{
/*
	if(threadIdx.x==0)
	{
	printf("GPU\n");
	for(int i=0;i<25;i++)
	{
		printf((i%5) == 0 ?"\n":" ");
		printf("%f",input[i]);
	}
	}
*/
		
	unsigned int size = width * width;
	unsigned int mask_size = mask_width * mask_width;
	unsigned int dest = blockIdx.x * blockDim.x + threadIdx.x ;
	float product = 0.0f;
	int count = 0;
	
	unsigned int start_ind = dest % size;
	unsigned int mask_ind = (dest / size) * filters * mask_size ;
	unsigned int mask_range = 0;

	if(dest < (width*width*op_filters)) // output filters
	{
#ifdef DEBUG_PRINT
		if(dest == ELEMENT)
		{
			printf("\nfilters=%d\n",filters);
		}
#endif	
	
		//int i = 0;
		while(count < filters) //** input filters = input depth **
		{
			mask_range = mask_ind + mask_size ;
			
#ifdef DEBUG_PRINT
		if(dest == ELEMENT)
		{
			printf("maskind=%d\n",mask_ind);
		}
#endif	
		    unsigned int tlbound = (start_ind/size) *size;
		    unsigned int brbound = tlbound+size;
			while(mask_ind<mask_range)//while(i<mask_size)
			{
				//int tempr = start_ind / width - (mask_width/2) + i/mask_width ;
				//int tempc = start_ind % width - (mask_width/2) + i%mask_width ; 

				int tempr = (start_ind / width) - (mask_width/2) + ((mask_ind % mask_size) / mask_width) ;
				int tempc = (int) start_ind % width - (mask_width/2) + mask_ind%mask_width ; //need to change?

#ifdef DEBUG_PRINT_2
				if(dest == ELEMENT)
				{
					printf("start_ind=%d  (mask_ind - (count * mask_size))  / mask_width = %d \ntempr=%d tempc=%d mask_width=%u\n",start_ind,(mask_ind - (count * mask_size))  / mask_width,tempr,tempc,mask_width);
				}
#endif

				if((tempr * width)>=tlbound && (tempr * width)<brbound && tempc>=0 && tempc<width) 
				{
					product+= mask[mask_ind]*input[(tempr * width) + tempc];
					//product+= mask[i]*input[(tempr * width) + tempc];
#ifdef DEBUG_PRINT				
				if(dest == ELEMENT)
					printf("mask[%u]=%0.2f input[%u][%u]=%0.2f  mask*input =%0.2f\n",mask_ind,mask[mask_ind],tempr,tempc,input[(tempr * width) + tempc],(float)mask[mask_ind]*input[(tempr * width) + tempc]);
#endif				
				}
				mask_ind++;
			}
			count++;
			start_ind +=  size ;
			

#ifdef DEBUG_PRINT
			if(dest==ELEMENT)
			{
				printf("startind = %d product = %0.2f\n",start_ind,product);
			}
#endif		
		}
		//add bias
		product+= bias[dest/size] ;
		//RELU
		if(product < 0.0f)
			product = 0.0;
		output[dest] = product;
#ifdef DEBUG_PRINT
			if(dest==ELEMENT)
			{
				printf("final product  for element =%d= %0.2f\n",ELEMENT,output[dest]);
			}
#endif
	}
}

void allocate(void **dest,size_t size,const char *message)
{
	cudaError_t err = cudaSuccess;
	
	err = cudaMalloc(dest,size);
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate memory for %s (error code %s)!\n",message,cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
}

void deallocate(float *dest,const char *message)
{
	cudaError_t err = cudaSuccess;
	err = cudaFree(dest);
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to free memory for %s (error code %s)!\n",message,cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}	
}

void memcopy(void *dest,const void *src, size_t size, cudaMemcpyKind kind,const char *message)
{
	cudaError_t err = cudaSuccess;
	err = cudaMemcpy(dest,src,size,kind);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy output for %s (error code %s)!\n", message,cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
}


__global__ void gprint(float *arr,size_t size,unsigned int width,const char *message)
{
	int th = blockIdx.x * blockDim.x + threadIdx.x ;
	if(th == 0)
	{
		printf("\n%s\n",message);
		for(unsigned int i = 0; i<size;i++)
		{
			printf((i%width) == 0 ?"\n":" ");
			printf("%0.2f",arr[i]);
		}
		printf("\n");
	}
}

void cprint(float *arr,size_t size,unsigned int width,const char *message,size_t limit)
{
		printf("\n%s\n",message);
		for(unsigned int i = 0; i<limit;i++)
		{
			printf((i%width) == 0 ?"\n":" ");
			printf("%0.2f",arr[i]);
		}
		printf("\n");
}


void execFirstLayer(float *output,float *input,float *mask)
{

#ifdef DEMO
	demo();
#else
	/* Input */
	printf("Sizeof red = %lu sizeof green =%lu sizeof blue = %lu\n",sizeof(red_array)/sizeof(red_array[0]),sizeof(green_array)/sizeof(green_array[0]),sizeof(blue_array)/sizeof(blue_array[0]));
	/*----- */
	cudaError_t err = cudaSuccess;
	
	float *h_input = NULL;
	unsigned int ipbytes = (sizeof(red_array) + sizeof(green_array) + sizeof(blue_array) ) / sizeof(float);
	unsigned int ipsize = (sizeof(red_array) + sizeof(green_array) + sizeof(blue_array) ) ;
	h_input = (float *)malloc(sizeof(red_array) + sizeof(green_array) + sizeof(blue_array) );
	
	if(h_input == NULL)
	{
		printf("error allocating\n");
	}
	
	int i =0;
	while(i < sizeof(red_array) / (sizeof(float)))
	{
		h_input[i] = blue_array[i];
		//h_input[i] = red_array[i];
		//printf("%0.2f ",h_input[i]);
		i++;
		
	}
	
	int j=0;
	while(j < (sizeof(green_array) / sizeof(float)))
	{	
		h_input[i] = green_array[j];
		i++;
		j++;
	}

	j=0;	
	while(j < (sizeof(blue_array) / sizeof(float)))
	{
		h_input[i] = red_array[j];
		//h_input[i] = blue_array[j];
		i++;
		j++;
	}
	
	/*
	printf("\ninput combined");
	
	for(unsigned int k= 0; k<ipbytes; k++)
	{
		printf((k%224) == 0 ?"\n":" ");
		printf("%0.2f",h_input[k]);
		//printf("k=%d\n",k);
	}
	*/
	
	float *d_conv11 = NULL;
	allocate((void **)&d_conv11,ipsize,"input copy to device");
	memcopy((void *)d_conv11,h_input,ipsize,cudaMemcpyHostToDevice, "input copy to device");
	 
		
	float *d_mconv11 = NULL;
	allocate((void **)&d_mconv11,sizeof(conv11),"Conv11 mask");
	printf("size of conv11 = %lu\n",sizeof(conv11)/sizeof(float));
	memcopy((void *)d_mconv11,(const void *)conv11,sizeof(conv11),cudaMemcpyHostToDevice,"Conv11 Mask");
	
	float *d_bconv11 = NULL;
	allocate((void **)&d_bconv11,sizeof(bias11),"Bias11");
	printf("size of bias11 = %lu\n",sizeof(bias11)/sizeof(float));
	memcopy((void *)d_bconv11,(const void *)bias11,sizeof(bias11),cudaMemcpyHostToDevice,"Conv11 Bias");
	
	
	/*
	for(int i=0;i<27;i++)
	{
		printf((i%3) == 0 ?"\n":" ");
		printf("%0.2f",conv11[i]);
	}
	*/
	
	/* Layer 1  */
	printf("\nLayer 1\n");
	unsigned int ip11_width = 224;
	unsigned int ip11_height = ip11_width * 3;
	unsigned int mask_width = 3;
	unsigned int mask_height = mask_width;
	unsigned int ipfilters = 3;
	unsigned int opfilters = 64;
	unsigned int op11_width = 224;
	unsigned int op11_height = op11_width * 64;
	
	float *d_op11 = NULL;
	allocate((void **)&d_op11,op11_width *op11_height * sizeof(float),"Output conv11");
	
	//conv1d(float *output,float *input,unsigned int width,unsigned int height,float *mask,unsigned int mask_width,unsigned int mask_height,unsigned int filters,unsigned int op_filters,float *bias)
	conv1d<<<10000,1024>>>(d_op11,d_conv11,ip11_width,ip11_height,d_mconv11,mask_width,mask_height,ipfilters,opfilters,d_bconv11);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();
		
	float *h_op11 = NULL;
	h_op11 = (float *)malloc(op11_width * op11_height * sizeof(float));
	
	memset(h_op11,0,op11_width * op11_height * sizeof(float));

	memcopy((void *)h_op11,d_op11,op11_width * op11_height * sizeof(float),cudaMemcpyDeviceToHost," host op11");
	//cprint(h_op11,op11_width * op11_height,op11_width,"Output from conv11",224*224);
	

	//Free Layer1 resources
	deallocate(d_conv11, "Conv11 input");
	deallocate(d_mconv11,"Conv11 mask ");
	deallocate(d_bconv11,"Conv11 bias ");
	free(h_op11);
	/*-----     */

	/* Layer 2  */
	
	printf("\nLayer 2\n");
	unsigned int ip12_width = 224;
	unsigned int ip12_depth = 64;
	unsigned int ip12_height = ip12_width * ip12_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op12_width = 224;
	unsigned int op12_depth = 64;
	unsigned int op12_height = op12_width * op12_depth;
	ipfilters = ip12_depth;
	opfilters = op12_depth;

	//layer 2 inputs
	//copy conv11 output to conv12 input	
	float *d_conv12 = NULL;
	allocate((void **)&d_conv12,op11_width *op11_height * sizeof(float),"Input conv12");
	memcopy((void *)d_conv12,d_op11,op11_width *op11_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv11 output to Conv12 input");

	//free previous output?
	 		
	float *d_mconv12 = NULL;
	allocate((void **)&d_mconv12,sizeof(conv12),"Conv12 mask");
	printf("size of conv12 = %lu\n",sizeof(conv12)/sizeof(float));
	memcopy((void *)d_mconv12,(const void *)conv12,sizeof(conv12),cudaMemcpyHostToDevice,"Conv12 Mask");
	
	float *d_bconv12 = NULL;
	allocate((void **)&d_bconv12,sizeof(bias12),"Bias12");
	printf("size of bias12 = %lu\n",sizeof(bias12)/sizeof(float));
	memcopy((void *)d_bconv12,(const void *)bias12,sizeof(bias12),cudaMemcpyHostToDevice,"Conv12 Bias");

	//layer 2 outputs	
	//device
	
	float *d_op12 = NULL;
	allocate((void **)&d_op12,op12_width *op12_height * sizeof(float),"Output conv12");
	
	//host
	float *h_op12 = NULL;
	h_op12 = (float *)malloc(op12_width * op12_height * sizeof(float));
	memset(h_op12,0,op12_width * op12_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op12,d_conv12,ip12_width,ip12_height,d_mconv12,mask_width,mask_height,ipfilters,opfilters,d_bconv12);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op12,d_op12,op12_width * op12_height * sizeof(float),cudaMemcpyDeviceToHost," host op12");
	//cprint(h_op12,op12_width * op12_height,op12_width,"Output from conv12",224*224);

	//Free Layer2 resources
	deallocate(d_conv12, "Conv12 input");
	deallocate(d_mconv12,"Conv12 mask ");
	deallocate(d_bconv12,"Conv12 bias ");
	free(h_op12);

	/*-----     */
	
	/* Maxpool */
	unsigned int ipm_width = op12_width;
	unsigned int ipm_depth = op12_depth;
	unsigned int ipm_height = ipm_width * op12_depth;

	unsigned int opm_width = op12_width/2;
	unsigned int opm_depth = ipm_depth;
	unsigned int opm_height = opm_width * op12_depth;
	ipfilters = ipm_depth;
	opfilters = ipfilters;
	unsigned int max_stride = 2;
	unsigned int max_mask_width =2;
	unsigned int max_mask_height = max_mask_width;
	
	//maxpool input
	float *d_imx12 = NULL;
	allocate((void **)&d_imx12,op12_width *op12_height * sizeof(float),"Input maxpool12");
	memcopy((void *)d_imx12,d_op12,op12_width *op12_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv12 output to Maxpool input");
	
	//maxpool output
	//device
	float *d_omx12 = NULL;
	allocate((void **)&d_omx12,opm_width *opm_height * sizeof(float),"Output maxpool12");
	//host
	float *h_omx12 = NULL;
	h_omx12 = (float *)malloc(opm_width *opm_height * sizeof(float));
	
	//kernel call
	maxpool<<<10000,1024>>>(d_omx12,opm_width,opm_height,d_imx12,ipm_width,ipm_height,ipfilters,max_stride,max_mask_width,max_mask_height);	
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_omx12,d_omx12,opm_width * opm_height * sizeof(float),cudaMemcpyDeviceToHost," host Maxpool 12");
	//cprint(h_omx12,opm_width * opm_height,opm_width,"Output from maxpool12",224*224);

	//printf("%0.2f %0.2f %0.2f %0.2f\n",h_omx12[0],h_omx12[1],h_omx12[224],h_omx12[225]); 
	//Free Maxpool Layer resources
	deallocate(d_imx12, "Max12 input");
	free(h_omx12);
	/*---------*/
	/* Layer 3  */
	printf("\nLayer 3\n");
	unsigned int ip21_width = 112;
	unsigned int ip21_depth = 64;
	unsigned int ip21_height = ip21_width * ip21_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op21_width = 112;
	unsigned int op21_depth = 128;
	unsigned int op21_height = op21_width * op21_depth;
	ipfilters = ip21_depth;
	opfilters = op21_depth;

	//layer 3 inputs
	//copy previous inputs	
	float *d_conv21 = NULL;
	allocate((void **)&d_conv21,op21_width *op21_height * sizeof(float),"Input conv21");
	memcopy((void *)d_conv21,d_omx12,opm_width *opm_height * sizeof(float),cudaMemcpyDeviceToDevice,"Maxpool12 output to Conv21 input");

	//free previous output?
	 		
	float *d_mconv21 = NULL;
	allocate((void **)&d_mconv21,sizeof(conv21),"Conv21 mask");
	printf("size of conv21 = %lu\n",sizeof(conv21)/sizeof(float));
	memcopy((void *)d_mconv21,(const void *)conv21,sizeof(conv21),cudaMemcpyHostToDevice,"Conv21 Mask");
	
	float *d_bconv21 = NULL;
	allocate((void **)&d_bconv21,sizeof(bias21),"Bias21");
	printf("size of bias21 = %lu\n",sizeof(bias21)/sizeof(float));
	memcopy((void *)d_bconv21,(const void *)bias21,sizeof(bias21),cudaMemcpyHostToDevice,"Conv21 Bias");

	//layer 3 outputs	
	//device
	
	float *d_op21 = NULL;
	allocate((void **)&d_op21,op21_width *op21_height * sizeof(float),"Output conv21");
	
	//host
	float *h_op21 = NULL;
	h_op21 = (float *)malloc(op21_width * op21_height * sizeof(float));
	memset(h_op21,0,op21_width * op21_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op21,d_conv21,ip21_width,ip21_height,d_mconv21,mask_width,mask_height,ipfilters,opfilters,d_bconv21);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op21,d_op21,op21_width * op21_height * sizeof(float),cudaMemcpyDeviceToHost," host op21");
	cprint(h_op21,op21_width * op21_height,op21_width,"Output from conv21",112);

	//Free Layer3 resources
	deallocate(d_conv21, "Conv21 input");
	deallocate(d_mconv21,"Conv21 mask ");
	deallocate(d_bconv21,"Conv21 bias ");
	free(h_op21);
	/*-----     */
	/* Layer 4  */

	printf("\nLayer 4\n");
	unsigned int ip22_width = 112;
	unsigned int ip22_depth = 128;
	unsigned int ip22_height = ip22_width * ip22_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op22_width = 112;
	unsigned int op22_depth = 128;
	unsigned int op22_height = op22_width * op22_depth;
	ipfilters = ip22_depth;
	opfilters = op22_depth;

	//layer 4 inputs
	//copy previous inputs	
	float *d_conv22 = NULL;
	allocate((void **)&d_conv22,ip22_width *ip22_height * sizeof(float),"Input conv22");
	memcopy((void *)d_conv22,d_op21,ip22_width *ip22_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv21 output to Conv22 input");

	//free previous output?
	 		
	float *d_mconv22 = NULL;
	allocate((void **)&d_mconv22,sizeof(conv22),"Conv22 mask");
	printf("size of conv22 = %lu\n",sizeof(conv22)/sizeof(float));
	memcopy((void *)d_mconv22,(const void *)conv22,sizeof(conv22),cudaMemcpyHostToDevice,"Conv22 Mask");
	
	float *d_bconv22 = NULL;
	allocate((void **)&d_bconv22,sizeof(bias22),"Bias22");
	printf("size of bias22 = %lu\n",sizeof(bias22)/sizeof(float));
	memcopy((void *)d_bconv22,(const void *)bias22,sizeof(bias22),cudaMemcpyHostToDevice,"Conv22 Bias");

	//layer 4 outputs	
	//device
	
	float *d_op22 = NULL;
	allocate((void **)&d_op22,op22_width *op22_height * sizeof(float),"Output conv22");
	
	//host
	float *h_op22 = NULL;
	h_op22 = (float *)malloc(op22_width * op22_height * sizeof(float));
	memset(h_op22,0,op22_width * op22_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op22,d_conv22,ip22_width,ip22_height,d_mconv22,mask_width,mask_height,ipfilters,opfilters,d_bconv22);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op22,d_op22,op22_width * op22_height * sizeof(float),cudaMemcpyDeviceToHost," host op22");
	cprint(h_op22,op22_width * op22_height,op22_width,"Output from conv22",112);

	//Free Layer4 resources
	deallocate(d_conv22, "Conv22 input");
	deallocate(d_mconv22,"Conv22 mask ");
	deallocate(d_bconv22,"Conv22 bias ");
	free(h_op22);
	/*---------*/
	/* Maxpool */
	printf("Maxpool output 22\n");
	 ipm_width = op22_width;
	 ipm_depth = op22_depth;
	 ipm_height = ipm_width * op22_depth;

	 opm_width = op22_width/2;
	 opm_depth = ipm_depth;
	 opm_height = opm_width * op22_depth;
	ipfilters = ipm_depth;
	opfilters = ipfilters;
	 max_stride = 2;
	 max_mask_width =2;
	 max_mask_height = max_mask_width;
	
	//maxpool input
	float *d_imx22 = NULL;
	allocate((void **)&d_imx22,op22_width *op22_height * sizeof(float),"Input maxpool22");
	memcopy((void *)d_imx22,d_op22,op22_width *op22_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv22 output to Maxpool input");
	
	//maxpool output
	//device
	float *d_omx22 = NULL;
	allocate((void **)&d_omx22,opm_width *opm_height * sizeof(float),"Output maxpool22");
	//host
	float *h_omx22 = NULL;
	h_omx22 = (float *)malloc(opm_width *opm_height * sizeof(float));
	
	//kernel call
	maxpool<<<10000,1024>>>(d_omx22,opm_width,opm_height,d_imx22,ipm_width,ipm_height,ipfilters,max_stride,max_mask_width,max_mask_height);	
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_omx22,d_omx22,opm_width * opm_height * sizeof(float),cudaMemcpyDeviceToHost," host Maxpool 22");
	//cprint(h_omx22,opm_width * opm_height,opm_width,"Output from maxpool22",112);

	//printf("%0.2f %0.2f %0.2f %0.2f\n",h_omx22[0],h_omx22[1],h_omx22[112],h_omx22[113]); 
	//Free Maxpool Layer resources
	deallocate(d_imx22, "Max22 input");
	free(h_omx22);
	/*---------*/
	/* Layer 5  */
	printf("\nLayer 5\n");
	unsigned int ip31_width = 56;
	unsigned int ip31_depth = 128;
	unsigned int ip31_height = ip31_width * ip31_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op31_width = 56;
	unsigned int op31_depth = 256;
	unsigned int op31_height = op31_width * op31_depth;
	ipfilters = ip31_depth;
	opfilters = op31_depth;
	unsigned int msize31 = mask_width * mask_height * ip31_depth * op31_depth;
	unsigned int bsize31 = op31_depth;

	float *conv31 = (float *)malloc(mask_width * mask_height * ip31_depth * op31_depth * sizeof(float));
	if(conv31 == NULL)
	{
		printf("Failed to allocate conv31\n");
		exit(EXIT_FAILURE);
	}
	float *bias31 = (float *)malloc(op31_depth*sizeof(float));
	if(bias31 == NULL)
	{
		printf("Failed to allocate bias31\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv31,mask_width * mask_height * ip31_depth * op31_depth,bias31,op31_depth,"weights/conv3_1_v.txt");

	//layer 5 inputs
	//copy previous inputs	
	float *d_conv31 = NULL;
	allocate((void **)&d_conv31,op31_width *op31_height * sizeof(float),"Input conv31");
	memcopy((void *)d_conv31,d_omx22,opm_width *opm_height * sizeof(float),cudaMemcpyDeviceToDevice,"Maxpool22 output to Conv31 input");

	//free previous output?
	 		
	float *d_mconv31 = NULL;
	allocate((void **)&d_mconv31,msize31 * sizeof(float),"Conv31 mask");
	printf("size of conv31 = %lu\n",(unsigned long)msize31);
	memcopy((void *)d_mconv31,(const void *)conv31,msize31*sizeof(float),cudaMemcpyHostToDevice,"Conv31 Mask");
	
	float *d_bconv31 = NULL;
	allocate((void **)&d_bconv31,bsize31 * sizeof(float),"Bias31");
	printf("size of bias31 = %lu\n",(unsigned long)bsize31);
	memcopy((void *)d_bconv31,(const void *)bias31,bsize31 * sizeof(float),cudaMemcpyHostToDevice,"Conv31 Bias");

	//layer 5 outputs	
	//device
	
	float *d_op31 = NULL;
	allocate((void **)&d_op31,op31_width *op31_height * sizeof(float),"Output conv31");
	
	//host
	float *h_op31 = NULL;
	h_op31 = (float *)malloc(op31_width * op31_height * sizeof(float));
	memset(h_op31,0,op31_width * op31_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op31,d_conv31,ip31_width,ip31_height,d_mconv31,mask_width,mask_height,ipfilters,opfilters,d_bconv31);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op31,d_op31,op31_width * op31_height * sizeof(float),cudaMemcpyDeviceToHost," host op31");
	//cprint(h_op31,op31_width * op31_height,op31_width,"Output from conv31",56);

	//Free Layer5 resources
	deallocate(d_conv31, "Conv31 input");
	deallocate(d_mconv31,"Conv31 mask ");
	deallocate(d_bconv31,"Conv31 bias ");
	free(h_op31);
	/*-----     */
	/* Layer 6  */

	printf("\nLayer 6\n");
	unsigned int ip32_width = 56;
	unsigned int ip32_depth = 256;
	unsigned int ip32_height = ip32_width * ip32_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op32_width = 56;
	unsigned int op32_depth = 256;
	unsigned int op32_height = op32_width * op32_depth;
	ipfilters = ip32_depth;
	opfilters = op32_depth;
	unsigned int msize32 = mask_width * mask_height * ip32_depth * op32_depth;
	unsigned int bsize32 = op32_depth;

	float *conv32 = (float *)malloc(mask_width * mask_height * ip32_depth * op32_depth * sizeof(float));
	if(conv32 == NULL)
	{
		printf("Failed to allocate conv32\n");
		exit(EXIT_FAILURE);
	}
	float *bias32 = (float *)malloc(op32_depth*sizeof(float));
	if(bias32 == NULL)
	{
		printf("Failed to allocate bias32\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv32,mask_width * mask_height * ip32_depth * op32_depth,bias32,op32_depth,"weights/conv3_2_v.txt");

	//layer 6 inputs
	//copy previous inputs	
	float *d_conv32 = NULL;
	allocate((void **)&d_conv32,op31_width *op31_height * sizeof(float),"Input conv32");
	memcopy((void *)d_conv32,d_op31,op31_width *op31_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv31 output to Conv32 input");

	//free previous output?
	 		
	float *d_mconv32 = NULL;
	allocate((void **)&d_mconv32,msize32 * sizeof(float),"Conv32 mask");
	printf("size of conv32 = %lu\n",(unsigned long)msize32);
	memcopy((void *)d_mconv32,(const void *)conv32,msize32 *sizeof(float),cudaMemcpyHostToDevice,"Conv32 Mask");
	
	float *d_bconv32 = NULL;
	allocate((void **)&d_bconv32,bsize32 * sizeof(float),"Bias32");
	printf("size of bias32 = %lu\n",(unsigned long)bsize32);
	memcopy((void *)d_bconv32,(const void *)bias32,bsize32 * sizeof(float),cudaMemcpyHostToDevice,"Conv32 Bias");

	//layer 6 outputs	
	//device
	
	float *d_op32 = NULL;
	allocate((void **)&d_op32,op32_width *op32_height * sizeof(float),"Output conv32");
	
	//host
	float *h_op32 = NULL;
	h_op32 = (float *)malloc(op32_width * op32_height * sizeof(float));
	memset(h_op32,0,op32_width * op32_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op32,d_conv32,ip32_width,ip32_height,d_mconv32,mask_width,mask_height,ipfilters,opfilters,d_bconv32);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op32,d_op32,op32_width * op32_height * sizeof(float),cudaMemcpyDeviceToHost," host op32");
	//cprint(h_op31,op31_width * op31_height,op31_width,"Output from conv31",56);

	//Free Layer6 resources
	deallocate(d_conv32, "Conv32 input");
	deallocate(d_mconv32,"Conv32 mask ");
	deallocate(d_bconv32,"Conv32 bias ");
	free(h_op32);
	/*-----     */
	/* Layer 7  */
	printf("\nLayer 7\n");
	unsigned int ip33_width = 56;
	unsigned int ip33_depth = 256;
	unsigned int ip33_height = ip33_width * ip33_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op33_width = 56;
	unsigned int op33_depth = 256;
	unsigned int op33_height = op33_width * op33_depth;
	ipfilters = ip33_depth;
	opfilters = op33_depth;
	unsigned int msize33 = mask_width * mask_height * ip33_depth * op33_depth;
	unsigned int bsize33 = op33_depth;

	float *conv33 = (float *)malloc(mask_width * mask_height * ip33_depth * op33_depth * sizeof(float));
	if(conv33 == NULL)
	{
		printf("Failed to allocate conv31\n");
		exit(EXIT_FAILURE);
	}
	float *bias33 = (float *)malloc(op33_depth*sizeof(float));
	if(bias33 == NULL)
	{
		printf("Failed to allocate bias31\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv33,mask_width * mask_height * ip33_depth * op33_depth,bias33,op33_depth,"weights/conv3_3_v.txt");

	//layer 7 inputs
	//copy previous inputs	
	float *d_conv33 = NULL;
	allocate((void **)&d_conv33,op32_width *op32_height * sizeof(float),"Input conv32");
	memcopy((void *)d_conv33,d_op32,op32_width *op32_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv32 output to Conv33 input");

	//free previous output?
	 		
	float *d_mconv33 = NULL;
	allocate((void **)&d_mconv33,msize33*sizeof(float),"Conv33 mask");
	printf("size of conv33 = %lu\n",(unsigned long)msize33);
	memcopy((void *)d_mconv33,(const void *)conv33,msize33*sizeof(float),cudaMemcpyHostToDevice,"Conv33 Mask");
	
	float *d_bconv33 = NULL;
	allocate((void **)&d_bconv33,bsize33*sizeof(float),"Bias33");
	printf("size of bias33 = %lu\n",(unsigned long)bsize33);
	memcopy((void *)d_bconv33,(const void *)bias33,bsize33*sizeof(float),cudaMemcpyHostToDevice,"Conv33 Bias");

	//layer 7 outputs	
	//device
	
	float *d_op33 = NULL;
	allocate((void **)&d_op33,op33_width *op33_height * sizeof(float),"Output conv33");
	
	//host
	float *h_op33 = NULL;
	h_op33 = (float *)malloc(op33_width * op33_height * sizeof(float));
	memset(h_op33,0,op33_width * op33_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op33,d_conv33,ip33_width,ip33_height,d_mconv33,mask_width,mask_height,ipfilters,opfilters,d_bconv33);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op33,d_op33,op33_width * op33_height * sizeof(float),cudaMemcpyDeviceToHost," host op33");
	//cprint(h_op33,op33_width * op33_height,op33_width,"Output from conv33",56);

	//Free Layer7 resources
	deallocate(d_conv33, "Conv33 input");
	deallocate(d_mconv33,"Conv33 mask ");
	deallocate(d_bconv33,"Conv33 bias ");
	free(h_op33);
	free(conv33);
	free(bias33);
	/*-----     */

	/* Maxpool */
	printf("Maxpool Layer after Conv33\n");
	 ipm_width = op33_width;
	 ipm_depth = op33_depth;
	 ipm_height = ipm_width * op33_depth;

	 opm_width = op33_width/2;
	 opm_depth = ipm_depth;
	 opm_height = opm_width * op33_depth;
	ipfilters = ipm_depth;
	opfilters = ipfilters;
	 max_stride = 2;
	 max_mask_width =2;
	 max_mask_height = max_mask_width;
	
	//maxpool input
	float *d_imx33 = NULL;
	allocate((void **)&d_imx33,op33_width *op33_height * sizeof(float),"Input maxpool33");
	memcopy((void *)d_imx33,d_op33,op33_width *op33_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv33 output to Maxpool input");
	
	//maxpool output
	//device
	float *d_omx33 = NULL;
	allocate((void **)&d_omx33,opm_width *opm_height * sizeof(float),"Output maxpool33");
	//host
	float *h_omx33 = NULL;
	h_omx33 = (float *)malloc(opm_width *opm_height * sizeof(float));
	
	//kernel call
	maxpool<<<10000,1024>>>(d_omx33,opm_width,opm_height,d_imx33,ipm_width,ipm_height,ipfilters,max_stride,max_mask_width,max_mask_height);	
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_omx33,d_omx33,opm_width * opm_height * sizeof(float),cudaMemcpyDeviceToHost," host Maxpool 33");
	//cprint(h_omx22,opm_width * opm_height,opm_width,"Output from maxpool22",112);

	//printf("%0.2f %0.2f %0.2f %0.2f\n",h_omx22[0],h_omx22[1],h_omx22[112],h_omx22[113]); 
	//Free Maxpool Layer resources
	deallocate(d_imx33, "Max33 input");
	free(h_omx33);
	/*---------*/
	/* Layer 8  */
	printf("\nLayer 8\n");
	unsigned int ip41_width = 28;
	unsigned int ip41_depth = 256;
	unsigned int ip41_height = ip41_width * ip41_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op41_width = 28;
	unsigned int op41_depth = 512;
	unsigned int op41_height = op41_width * op41_depth;
	ipfilters = ip41_depth;
	opfilters = op41_depth;
	unsigned int msize41 = mask_width * mask_height * ip41_depth * op41_depth;
	unsigned int bsize41 = op41_depth;

	//layer 8 inputs
	//copy previous inputs	
	float *conv41 = (float *)malloc(mask_width * mask_height * ip41_depth * op41_depth * sizeof(float));
	if(conv41 == NULL)
	{
		printf("Failed to allocate conv41\n");
		exit(EXIT_FAILURE);
	}
	float *bias41 = (float *)malloc(op41_depth*sizeof(float));
	if(bias41 == NULL)
	{
		printf("Failed to allocate bias41\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv41,mask_width * mask_height * ip41_depth * op41_depth,bias41,op41_depth,"weights/conv4_1_v.txt");
	
	float *d_conv41 = NULL;
	allocate((void **)&d_conv41,ip41_width *ip41_height * sizeof(float),"Input conv41");
	memcopy((void *)d_conv41,d_omx33,opm_width *opm_height * sizeof(float),cudaMemcpyDeviceToDevice,"Maxpool33 output to Conv41 input");

	//free previous output?
	 		
	float *d_mconv41 = NULL;
	allocate((void **)&d_mconv41,msize41*sizeof(float),"Conv41 mask");
	printf("size of conv41 = %lu\n",(unsigned long)mask_width * mask_height * ip41_depth * op41_depth);
	memcopy((void *)d_mconv41,(const void *)conv41,msize41*sizeof(float),cudaMemcpyHostToDevice,"Conv41 Mask");
	
	float *d_bconv41 = NULL;
	allocate((void **)&d_bconv41,bsize41*sizeof(float),"Bias41");
	printf("size of bias41 = %lu\n",(unsigned long)op41_depth);
	memcopy((void *)d_bconv41,(const void *)bias41,bsize41*sizeof(float),cudaMemcpyHostToDevice,"Conv41 Bias");

	//layer 8 outputs	
	//device
	
	float *d_op41 = NULL;
	allocate((void **)&d_op41,op41_width *op41_height * sizeof(float),"Output conv41");
	
	//host
	float *h_op41 = NULL;
	h_op41 = (float *)malloc(op41_width * op41_height * sizeof(float));
	memset(h_op41,0,op41_width * op41_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op41,d_conv41,ip41_width,ip41_height,d_mconv41,mask_width,mask_height,ipfilters,opfilters,d_bconv41);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op41,d_op41,op41_width * op41_height * sizeof(float),cudaMemcpyDeviceToHost," host op41");
	//cprint(h_op41,op41_width * op41_height,op41_width,"Output from conv41",28);

	//Free Layer8 resources
	deallocate(d_conv41, "Conv41 input");
	deallocate(d_mconv41,"Conv41 mask ");
	deallocate(d_bconv41,"Conv41 bias ");
	free(conv41);
	free(bias41);
	/*-----     */
	/* Layer 9  */
	printf("\nLayer 9\n");
	unsigned int ip42_width = 28;
	unsigned int ip42_depth = 512;
	unsigned int ip42_height = ip42_width * ip42_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op42_width = 28;
	unsigned int op42_depth = 512;
	unsigned int op42_height = op42_width * op42_depth;
	ipfilters = ip42_depth;
	opfilters = op42_depth;
	unsigned int msize42 = mask_width * mask_height * ip42_depth * op42_depth;
	unsigned int bsize42 = op42_depth;

	//layer 9 inputs
	//copy previous inputs	
	float *d_conv42 = NULL;
	allocate((void **)&d_conv42,op42_width *op42_height * sizeof(float),"Input conv42");
	memcopy((void *)d_conv42,d_op41,op41_width *op41_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv41 output to Conv42 input");

	//free previous output?
	
	float *conv42 = (float *)malloc(mask_width * mask_height * ip42_depth * op42_depth * sizeof(float));
	if(conv42 == NULL)
	{
		printf("Failed to allocate conv42\n");
		exit(EXIT_FAILURE);
	}

	float *bias42 = (float *)malloc(op42_depth * sizeof(float));
	if(bias41 == NULL)
	{
		printf("Failed to allocate bias41\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv42,mask_width * mask_height * ip42_depth * op42_depth,bias42,op42_depth,"weights/conv4_2_v.txt");
	 		
	float *d_mconv42 = NULL;
	allocate((void **)&d_mconv42,msize42*sizeof(float),"Conv42 mask");
	printf("size of conv42 = %lu\n",(unsigned long)msize42);
	
	memcopy((void *)d_mconv42,(const void *)conv42,msize42*sizeof(float),cudaMemcpyHostToDevice,"Conv42 Mask");
	
	float *d_bconv42 = NULL;
	allocate((void **)&d_bconv42,bsize42*sizeof(float),"Bias42");
	printf("size of bias42 = %lu\n",(unsigned long)bsize42);
	memcopy((void *)d_bconv42,(const void *)bias42,bsize42*sizeof(float),cudaMemcpyHostToDevice,"Conv42 Bias");

	//layer 9 outputs	
	//device
	
	float *d_op42 = NULL;
	allocate((void **)&d_op42,op42_width *op42_height * sizeof(float),"Output conv42");
	
	//host
	float *h_op42 = NULL;
	h_op42 = (float *)malloc(op42_width * op42_height * sizeof(float));
	memset(h_op42,0,op42_width * op42_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op42,d_conv42,ip42_width,ip42_height,d_mconv42,mask_width,mask_height,ipfilters,opfilters,d_bconv42);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op42,d_op42,op42_width * op42_height * sizeof(float),cudaMemcpyDeviceToHost," host op42");
	//cprint(h_op31,op31_width * op31_height,op31_width,"Output from conv31",56);

	//Free Layer9 resources
	deallocate(d_conv42, "Conv42 input");
	deallocate(d_mconv42,"Conv42 mask ");
	deallocate(d_bconv42,"Conv42 bias ");
	free(conv42);
	free(bias42);
	/*-----     */
	/* Layer 10 */

	printf("\nLayer 10\n");
	unsigned int ip43_width = 28;
	unsigned int ip43_depth = 512;
	unsigned int ip43_height = ip43_width * ip43_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op43_width = 28;
	unsigned int op43_depth = 512;
	unsigned int op43_height = op43_width * op43_depth;
	ipfilters = ip43_depth;
	opfilters = op43_depth;
	unsigned int msize43 = mask_width * mask_height * ip43_depth * op43_depth;
	unsigned int bsize43 = op43_depth;

	//layer 10 inputs
	//copy previous inputs	
	float *d_conv43 = NULL;
	allocate((void **)&d_conv43,op42_width *op42_height * sizeof(float),"Input conv43");
	memcopy((void *)d_conv43,d_op42,op42_width *op42_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv42 output to Conv43 input");
	//free previous output?
	float *conv43 = (float *)malloc(mask_width * mask_height * ip43_depth * op43_depth * sizeof(float));
	if(conv43 == NULL)
	{
		printf("Failed to allocate conv43\n");
		exit(EXIT_FAILURE);
	}

	float *bias43 = (float *)malloc(op43_depth * sizeof(float));
	if(bias43 == NULL)
	{
		printf("Failed to allocate bias43\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv43,mask_width * mask_height * ip43_depth * op43_depth,bias43,op43_depth,"weights/conv4_3_v.txt");
	 		
	float *d_mconv43 = NULL;

	allocate((void **)&d_mconv43,msize43*sizeof(float),"Conv43 mask");

	printf("size of conv43 = %lu\n",(unsigned long)msize43);
	
	memcopy((void *)d_mconv43,(const void *)conv43,msize43*sizeof(float),cudaMemcpyHostToDevice,"Conv43 Mask");

	float *d_bconv43 = NULL;

	allocate((void **)&d_bconv43,bsize43*sizeof(float),"Bias43");

	printf("size of bias43 = %lu\n",(unsigned long)bsize43);

	memcopy((void *)d_bconv43,(const void *)bias43,bsize43*sizeof(float),cudaMemcpyHostToDevice,"Conv43 Bias");

	//layer 10 outputs	
	//device
	float *d_op43 = NULL;
	allocate((void **)&d_op43,op43_width *op43_height * sizeof(float),"Output conv43");

	//host
	float *h_op43 = NULL;
	h_op43 = (float *)malloc(op43_width * op43_height * sizeof(float));
	memset(h_op43,0,op43_width * op43_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op43,d_conv43,ip43_width,ip43_height,d_mconv43,mask_width,mask_height,ipfilters,opfilters,d_bconv43);

	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    
    cudaThreadSynchronize();
	cudaDeviceSynchronize();
	
	memcopy((void *)h_op43,d_op43,op43_width * op43_height * sizeof(float),cudaMemcpyDeviceToHost," host op43");

	//cprint(h_op31,op31_width * op31_height,op31_width,"Output from conv31",56);

	//Free Layer10 resources
	deallocate(d_conv43, "Conv43 input");
	deallocate(d_mconv43,"Conv43 mask ");
	deallocate(d_bconv43,"Conv43 bias ");
	free(conv43);
	free(bias43);

	/*-----     */
	
	/* Maxpool */
	printf("Maxpool Layer after Conv43\n");
	 ipm_width = op43_width;
	 ipm_depth = op43_depth;
	 ipm_height = ipm_width * op43_depth;

	 opm_width = op43_width/2;
	 opm_depth = ipm_depth;
	 opm_height = opm_width * op43_depth;
	ipfilters = ipm_depth;
	opfilters = ipfilters;
	 max_stride = 2;
	 max_mask_width =2;
	 max_mask_height = max_mask_width;
	
	//maxpool input
	float *d_imx43 = NULL;
	allocate((void **)&d_imx43,op43_width *op43_height * sizeof(float),"Input maxpool43");
	memcopy((void *)d_imx43,d_op43,op43_width *op43_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv43 output to Maxpool input");
	
	//maxpool output
	//device
	float *d_omx43 = NULL;
	allocate((void **)&d_omx43,opm_width *opm_height * sizeof(float),"Output maxpool43");
	//host
	float *h_omx43 = NULL;
	h_omx43 = (float *)malloc(opm_width *opm_height * sizeof(float));
	
	//kernel call
	maxpool<<<10000,1024>>>(d_omx43,opm_width,opm_height,d_imx43,ipm_width,ipm_height,ipfilters,max_stride,max_mask_width,max_mask_height);	
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_omx43,d_omx43,opm_width * opm_height * sizeof(float),cudaMemcpyDeviceToHost," host Maxpool 43");
	//cprint(h_omx43,opm_width * opm_height,opm_width,"Output from maxpool43",112);

	//printf("%0.2f %0.2f %0.2f %0.2f\n",h_omx22[0],h_omx22[1],h_omx22[112],h_omx22[113]); 
	//Free Maxpool Layer resources
	deallocate(d_imx43, "Max43 input");
	free(h_omx43);
	/*---------*/
	/* Layer 11 */
	printf("\nLayer 11\n");
	unsigned int ip51_width = 14;
	unsigned int ip51_depth = 512;
	unsigned int ip51_height = ip51_width * ip51_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op51_width = 14;
	unsigned int op51_depth = 512;
	unsigned int op51_height = op51_width * op51_depth;
	ipfilters = ip51_depth;
	opfilters = op51_depth;
	unsigned int msize51 = mask_width * mask_height * ip51_depth * op51_depth;
	unsigned int bsize51 = op51_depth;

	//layer 11 inputs
	//copy previous inputs	
	float *conv51 = (float *)malloc(mask_width * mask_height * ip51_depth * op51_depth * sizeof(float));
	if(conv51 == NULL)
	{
		printf("Failed to allocate conv51\n");
		exit(EXIT_FAILURE);
	}
	float *bias51 = (float *)malloc(op51_depth*sizeof(float));
	if(bias51 == NULL)
	{
		printf("Failed to allocate bias51\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv51,mask_width * mask_height * ip51_depth * op51_depth,bias51,op51_depth,"weights/conv5_1_v.txt");
	
	float *d_conv51 = NULL;
	allocate((void **)&d_conv51,ip51_width *ip51_height * sizeof(float),"Input conv51");
	memcopy((void *)d_conv51,d_omx43,opm_width *opm_height * sizeof(float),cudaMemcpyDeviceToDevice,"Maxpool43 output to Conv51 input");

	//free previous output?
	 		
	float *d_mconv51 = NULL;
	allocate((void **)&d_mconv51,msize51*sizeof(float),"Conv51 mask");
	printf("size of conv51 = %lu\n",(unsigned long)mask_width * mask_height * ip51_depth * op51_depth);
	memcopy((void *)d_mconv51,(const void *)conv51,msize51*sizeof(float),cudaMemcpyHostToDevice,"Conv51 Mask");
	
	float *d_bconv51 = NULL;
	allocate((void **)&d_bconv51,bsize51*sizeof(float),"Bias51");
	printf("size of bias51 = %lu\n",(unsigned long)op51_depth);
	memcopy((void *)d_bconv51,(const void *)bias51,bsize51*sizeof(float),cudaMemcpyHostToDevice,"Conv51 Bias");

	//layer 11 outputs	
	//device
	
	float *d_op51 = NULL;
	allocate((void **)&d_op51,op51_width *op51_height * sizeof(float),"Output conv51");
	
	//host
	float *h_op51 = NULL;
	h_op51 = (float *)malloc(op51_width * op51_height * sizeof(float));
	memset(h_op51,0,op51_width * op51_height * sizeof(float));

	//kernel launch
	conv1d<<<10000,1024>>>(d_op51,d_conv51,ip51_width,ip51_height,d_mconv51,mask_width,mask_height,ipfilters,opfilters,d_bconv51);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op51,d_op51,op51_width * op51_height * sizeof(float),cudaMemcpyDeviceToHost," host op51");
	//cprint(h_op41,op41_width * op41_height,op41_width,"Output from conv41",28);

	//Free Layer11 resources
	deallocate(d_conv51, "Conv51 input");
	deallocate(d_mconv51,"Conv51 mask ");
	deallocate(d_bconv51,"Conv51 bias ");
	free(conv51);
	free(bias51);
	
	/*-----     */

	/* Layer 12 */
	printf("\nLayer 12\n");
	unsigned int ip52_width = 14;
	unsigned int ip52_depth = 512;
	unsigned int ip52_height = ip52_width * ip52_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op52_width = 14;
	unsigned int op52_depth = 512;
	unsigned int op52_height = op52_width * op52_depth;
	ipfilters = ip52_depth;
	opfilters = op52_depth;
	unsigned int msize52 = mask_width * mask_height * ip52_depth * op52_depth;
	unsigned int bsize52 = op52_depth;
	
	//layer 12 inputs
	//copy previous inputs	
	float *d_conv52 = NULL;
	allocate((void **)&d_conv52,ip52_width *ip52_height * sizeof(float),"Input conv52");
	memcopy((void *)d_conv52,d_op51,op51_width *op51_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv51 output to Conv52 input");

	//free previous output?	
	float *conv52 = (float *)malloc(mask_width * mask_height * ip52_depth * op52_depth * sizeof(float));
	if(conv52 == NULL)
	{
		printf("Failed to allocate conv52\n");
		exit(EXIT_FAILURE);
	}

	float *bias52 = (float *)malloc(op52_depth * sizeof(float));
	if(bias51 == NULL)
	{
		printf("Failed to allocate bias41\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv52,mask_width * mask_height * ip52_depth * op52_depth,bias52,op52_depth,"weights/conv5_2_v.txt");
	float *d_mconv52 = NULL;
	allocate((void **)&d_mconv52,msize52*sizeof(float),"Conv52 mask");
	printf("size of conv52 = %lu\n",(unsigned long)msize52);
	memcopy((void *)d_mconv52,(const void *)conv52,msize52*sizeof(float),cudaMemcpyHostToDevice,"Conv52 Mask");
	float *d_bconv52 = NULL;
	allocate((void **)&d_bconv52,bsize52*sizeof(float),"Bias52");
	printf("size of bias52 = %lu\n",(unsigned long)bsize52);

	memcopy((void *)d_bconv52,(const void *)bias52,bsize52*sizeof(float),cudaMemcpyHostToDevice,"Conv52 Bias");

	//layer 12 outputs	

	//device

	float *d_op52 = NULL;
	allocate((void **)&d_op52,op52_width *op52_height * sizeof(float),"Output conv52");

	//host
	float *h_op52 = NULL;
	h_op52 = (float *)malloc(op52_width * op52_height * sizeof(float));
	memset(h_op52,0,op52_width * op52_height * sizeof(float));

	//kernel launch

	conv1d<<<10000,1024>>>(d_op52,d_conv52,ip52_width,ip52_height,d_mconv52,mask_width,mask_height,ipfilters,opfilters,d_bconv52);

	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_op52,d_op52,op52_width * op52_height * sizeof(float),cudaMemcpyDeviceToHost," host op52");

	//cprint(h_op31,op31_width * op31_height,op31_width,"Output from conv31",56);

	//Free Layer12 resources
	deallocate(d_conv52, "Conv52 input");
	deallocate(d_mconv52,"Conv52 mask ");
	deallocate(d_bconv52,"Conv52 bias ");
	free(conv52);
	free(bias52);
	
	/*-----     */
	/* Layer 13 */
	
	printf("\nLayer 13\n");
	unsigned int ip53_width = 14;
	unsigned int ip53_depth = 512;
	unsigned int ip53_height = ip53_width * ip53_depth;
	mask_width = 3;
	mask_height = mask_width;
	unsigned int op53_width = 14;
	unsigned int op53_depth = 512;
	unsigned int op53_height = op53_width * op53_depth;
	ipfilters = ip53_depth;
	opfilters = op53_depth;
	unsigned int msize53 = mask_width * mask_height * ip53_depth * op53_depth;
	unsigned int bsize53 = op53_depth;

	//layer 13 inputs
	//copy previous inputs	
	float *d_conv53 = NULL;
	allocate((void **)&d_conv53,op52_width *op52_height * sizeof(float),"Input conv53");
	memcopy((void *)d_conv53,d_op52,op52_width *op52_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv52 output to Conv53 input");
	//free previous output?
	float *conv53 = (float *)malloc(mask_width * mask_height * ip53_depth * op53_depth * sizeof(float));
	if(conv53 == NULL)
	{
		printf("Failed to allocate conv53\n");
		exit(EXIT_FAILURE);
	}

	float *bias53 = (float *)malloc(op53_depth * sizeof(float));
	if(bias53 == NULL)
	{
		printf("Failed to allocate bias53\n");
		exit(EXIT_FAILURE);
	}

	read_two_file(conv53,mask_width * mask_height * ip53_depth * op53_depth,bias53,op53_depth,"weights/conv5_3_v.txt");
	 		
	float *d_mconv53 = NULL;
	allocate((void **)&d_mconv53,msize53*sizeof(float),"Conv53 mask");
	printf("size of conv53 = %lu\n",(unsigned long)msize53);
	
	memcopy((void *)d_mconv53,(const void *)conv53,msize53*sizeof(float),cudaMemcpyHostToDevice,"Conv53 Mask");
	float *d_bconv53 = NULL;
	allocate((void **)&d_bconv53,bsize53*sizeof(float),"Bias53");
	printf("size of bias53 = %lu\n",(unsigned long)bsize53);
	memcopy((void *)d_bconv53,(const void *)bias53,bsize53*sizeof(float),cudaMemcpyHostToDevice,"Conv53 Bias");
	//layer 13 outputs	
	//device
	float *d_op53 = NULL;
	allocate((void **)&d_op53,op53_width *op53_height * sizeof(float),"Output conv53");
	//host
	float *h_op53 = NULL;
	h_op53 = (float *)malloc(op53_width * op53_height * sizeof(float));
	memset(h_op53,0,op53_width * op53_height * sizeof(float));
	//kernel launch
	conv1d<<<10000,1024>>>(d_op53,d_conv53,ip53_width,ip53_height,d_mconv53,mask_width,mask_height,ipfilters,opfilters,d_bconv53);
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    
    cudaThreadSynchronize();
	cudaDeviceSynchronize();
	
	memcopy((void *)h_op53,d_op53,op53_width * op53_height * sizeof(float),cudaMemcpyDeviceToHost," host op53");
	//cprint(h_op51,op51_width * op51_height,op51_width,"Output from conv51",56);
	//Free Layer13 resources
	deallocate(d_conv53, "Conv53 input");
	deallocate(d_mconv53,"Conv53 mask ");
	deallocate(d_bconv53,"Conv53 bias ");
	free(conv53);
	free(bias53);

	/*-----     */
	
	/* Maxpool */
	printf("Maxpool Layer after Conv43\n");
	 ipm_width = op53_width;
	 ipm_depth = op53_depth;
	 ipm_height = ipm_width * op53_depth;

	 opm_width = op53_width/2;
	 opm_depth = ipm_depth;
	 opm_height = opm_width * op53_depth;
	ipfilters = ipm_depth;
	opfilters = ipfilters;
	 max_stride = 2;
	 max_mask_width =2;
	 max_mask_height = max_mask_width;
	
	//maxpool input
	float *d_imx53 = NULL;
	allocate((void **)&d_imx53,op53_width *op53_height * sizeof(float),"Input maxpool53");
	memcopy((void *)d_imx53,d_op53,op53_width *op53_height * sizeof(float),cudaMemcpyDeviceToDevice,"Conv53 output to Maxpool input");
	
	//maxpool output
	//device
	float *d_omx53 = NULL;
	allocate((void **)&d_omx53,opm_width *opm_height * sizeof(float),"Output maxpool53");
	//host
	float *h_omx53 = NULL;
	h_omx53 = (float *)malloc(opm_width *opm_height * sizeof(float));
	
	//kernel call
	maxpool<<<10000,1024>>>(d_omx53,opm_width,opm_height,d_imx53,ipm_width,ipm_height,ipfilters,max_stride,max_mask_width,max_mask_height);	
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaThreadSynchronize();
	cudaDeviceSynchronize();

	memcopy((void *)h_omx53,d_omx53,opm_width * opm_height * sizeof(float),cudaMemcpyDeviceToHost," host Maxpool 53");
	//cprint(h_omx53,opm_width * opm_height,opm_width,"Output from maxpool53",112);

	//printf("%0.2f %0.2f %0.2f %0.2f\n",h_omx22[0],h_omx22[1],h_omx22[112],h_omx22[113]); 
	//Free Maxpool Layer resources
	deallocate(d_imx53, "Max53 input");
	free(h_omx53);
	/*---------*/

	/* Layer 14 */
	printf("\nFC 6 Layer\n");
	unsigned int ifc6_width = 7;
	unsigned int ifc6_depth = 512;
	unsigned int ifc6_height = ifc6_width * ifc6_depth;
	ipfilters = 512;
	opfilters = 4096;
	unsigned int ofc6_width = 1;
	unsigned int ofc6_depth = 4096;
	unsigned int ofc6_height = ofc6_width * ofc6_depth;
	unsigned int msizefc6 = ifc6_width * ifc6_height * opfilters;
	unsigned int bsizefc6 = opfilters;

	//layer 14 inputs
	//copy previous inputs	
	float *d_fc6 = NULL;
	allocate((void **)&d_fc6,ifc6_width *ifc6_height * sizeof(float),"Input FC 6");
	memcopy((void *)d_fc6,d_omx53,opm_width *opm_height * sizeof(float),cudaMemcpyDeviceToDevice,"Maxpool53 to FC6 input");
	//free previous output?
	float *convfc6 = (float *)malloc(msizefc6 * sizeof(float));
	if(convfc6 == NULL)
	{
		printf("Failed to allocate convfc6\n");
		exit(EXIT_FAILURE);
	}

	float *biasfc6 = (float *)malloc(bsizefc6 * sizeof(float));
	if(biasfc6 == NULL)
	{
		printf("Failed to allocate biasfc6\n");
		exit(EXIT_FAILURE);
	}

	read_fc_file(convfc6,msizefc6,biasfc6,bsizefc6,"weights/fc6_v.txt");
	 		
	float *d_mconvfc6 = NULL;
	allocate((void **)&d_mconvfc6,msizefc6*sizeof(float),"FC 6 mask");
	printf("size of FC6 mask = %lu\n",(unsigned long)msizefc6);
	memcopy((void *)d_mconvfc6,(const void *)convfc6,msizefc6*sizeof(float),cudaMemcpyHostToDevice,"FC 6 Mask");
	
	float *d_bconvfc6 = NULL;
	allocate((void **)&d_bconvfc6,bsizefc6*sizeof(float),"Biasfc6");
	printf("size of biasfc6 = %lu\n",(unsigned long)bsizefc6);
	memcopy((void *)d_bconvfc6,(const void *)biasfc6,bsizefc6*sizeof(float),cudaMemcpyHostToDevice,"Convfc6 Bias");
	//layer 14 outputs	
	//device
	float *d_ofc6 = NULL;
	allocate((void **)&d_ofc6,ofc6_width *ofc6_height * sizeof(float),"Output fc6");
	//host
	float *h_ofc6 = NULL;
	h_ofc6 = (float *)malloc(ofc6_width * ofc6_height * sizeof(float));
	memset(h_ofc6,0,ofc6_width * ofc6_height * sizeof(float));
	
	fclayer<<<10000,1024>>>(d_ofc6, ofc6_width,ofc6_height, ofc6_depth, d_fc6,ifc6_width,ifc6_height,ifc6_depth,d_mconvfc6,opfilters,d_bconvfc6);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    
    cudaThreadSynchronize();
	cudaDeviceSynchronize();
	
	memcopy((void *)h_ofc6,d_ofc6,ofc6_width * ofc6_height * sizeof(float),cudaMemcpyDeviceToHost," host ofc6");
	//cprint(h_ofc6,ofc6_width * ofc6_height,ofc6_width,"Output from FC6",opfilters);
	
	//Free Layer14 resources
	deallocate(d_fc6, "Convfc6 input");
	deallocate(d_mconvfc6,"Convfc6 mask ");
	deallocate(d_bconvfc6,"Convfc6 bias ");
	free(convfc6);
	free(biasfc6);
	
	
	/*-----     */
	/* Layer 15 */

	printf("\nFC 7 Layer\n");
	unsigned int ifc7_width = 1;
	unsigned int ifc7_depth = 4096;
	unsigned int ifc7_height = ifc7_width * ifc7_depth;
	ipfilters = 4096;
	opfilters = 4096;
	unsigned int ofc7_width = 1;
	unsigned int ofc7_depth = 4096;
	unsigned int ofc7_height = ofc7_width * ofc7_depth;
	unsigned int msizefc7 = ifc7_width * ifc7_height * opfilters;
	unsigned int bsizefc7 = opfilters;

	//layer 15 inputs
	//copy previous inputs	
	float *d_fc7 = NULL;
	allocate((void **)&d_fc7,ifc7_width *ifc7_height * sizeof(float),"Input FC 7");
	memcopy((void *)d_fc7,d_ofc6,ifc7_width *ifc7_height * sizeof(float),cudaMemcpyDeviceToDevice,"FC6 to FC7 input");
	//free previous output?
	float *convfc7 = (float *)malloc(msizefc7 * sizeof(float));
	if(convfc7 == NULL)
	{
		printf("Failed to allocate convfc7\n");
		exit(EXIT_FAILURE);
	}

	float *biasfc7 = (float *)malloc(bsizefc7 * sizeof(float));
	if(biasfc7 == NULL)
	{
		printf("Failed to allocate biasfc7\n");
		exit(EXIT_FAILURE);
	}

	read_fc_file(convfc7,msizefc7,biasfc7,bsizefc7,"weights/fc7_v.txt");
	 		
	float *d_mconvfc7 = NULL;
	allocate((void **)&d_mconvfc7,msizefc7*sizeof(float),"FC 7 mask");
	printf("size of FC7 mask = %lu\n",(unsigned long)msizefc7);
	memcopy((void *)d_mconvfc7,(const void *)convfc7,msizefc7*sizeof(float),cudaMemcpyHostToDevice,"FC 7 Mask");
	
	float *d_bconvfc7 = NULL;
	allocate((void **)&d_bconvfc7,bsizefc7*sizeof(float),"Biasfc7");
	printf("size of biasfc7 = %lu\n",(unsigned long)bsizefc7);
	memcopy((void *)d_bconvfc7,(const void *)biasfc7,bsizefc7*sizeof(float),cudaMemcpyHostToDevice,"Convfc7 Bias");
	//layer 15 outputs	
	//device
	float *d_ofc7 = NULL;
	allocate((void **)&d_ofc7,ofc7_width *ofc7_height * sizeof(float),"Output fc7");
	//host
	float *h_ofc7 = NULL;
	h_ofc7 = (float *)malloc(ofc7_width * ofc7_height * sizeof(float));
	memset(h_ofc7,0,ofc7_width * ofc7_height * sizeof(float));
	
	fclayer<<<10000,1024>>>(d_ofc7, ofc7_width,ofc7_height, ofc7_depth, d_fc7,ifc7_width,ifc7_height,ifc7_depth,d_mconvfc7,opfilters,d_bconvfc7);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    
    cudaThreadSynchronize();
	cudaDeviceSynchronize();
	
	memcopy((void *)h_ofc7,d_ofc7,ofc7_width * ofc7_height * sizeof(float),cudaMemcpyDeviceToHost," host ofc7");
	//cprint(h_ofc7,ofc7_width * ofc7_height,ofc7_width,"Output from FC7",opfilters);
	
	//Free Layer15 resources
	deallocate(d_fc7, "Convfc7 input");
	deallocate(d_mconvfc7,"Convfc7 mask ");
	deallocate(d_bconvfc7,"Convfc7 bias ");
	free(convfc7);
	free(biasfc7);
	
	/*-----     */
	/* Layer 16 */
	printf("\nFC 8 Layer\n");
	unsigned int ifc8_width = 1;
	unsigned int ifc8_depth = 4096;
	unsigned int ifc8_height = ifc8_width * ifc8_depth;
	ipfilters = 4096;
	opfilters = 1000;
	unsigned int ofc8_width = 1;
	unsigned int ofc8_depth = 1000;
	unsigned int ofc8_height = ofc8_width * ofc8_depth;
	unsigned int msizefc8 = ifc8_width * ifc8_height * opfilters;
	unsigned int bsizefc8 = opfilters;

	//layer 15 inputs
	//copy previous inputs	
	float *d_fc8 = NULL;
	allocate((void **)&d_fc8,ifc8_width *ifc8_height * sizeof(float),"Input FC 8");
	memcopy((void *)d_fc8,d_ofc7,ifc8_width *ifc8_height * sizeof(float),cudaMemcpyDeviceToDevice,"FC7 to FC8 input");
	//free previous output?
	float *convfc8 = (float *)malloc(msizefc8 * sizeof(float));
	if(convfc8 == NULL)
	{
		printf("Failed to allocate convfc8\n");
		exit(EXIT_FAILURE);
	}

	float *biasfc8 = (float *)malloc(bsizefc8 * sizeof(float));
	if(biasfc8 == NULL)
	{
		printf("Failed to allocate biasfc8\n");
		exit(EXIT_FAILURE);
	}

	read_fc_file(convfc8,msizefc8,biasfc8,bsizefc8,"weights/fc8_v.txt");
	 		
	float *d_mconvfc8 = NULL;
	allocate((void **)&d_mconvfc8,msizefc8*sizeof(float),"FC 8 mask");
	printf("size of FC8 mask = %lu\n",(unsigned long)msizefc8);
	memcopy((void *)d_mconvfc8,(const void *)convfc8,msizefc8*sizeof(float),cudaMemcpyHostToDevice,"FC 8 Mask");
	
	float *d_bconvfc8 = NULL;
	allocate((void **)&d_bconvfc8,bsizefc8*sizeof(float),"Biasfc8");
	printf("size of biasfc8 = %lu\n",(unsigned long)bsizefc8);
	memcopy((void *)d_bconvfc8,(const void *)biasfc8,bsizefc8*sizeof(float),cudaMemcpyHostToDevice,"Convfc8 Bias");
	//layer 16 outputs	
	//device
	float *d_ofc8 = NULL;
	allocate((void **)&d_ofc8,ofc8_width *ofc8_height * sizeof(float),"Output fc8");
	//host
	float *h_ofc8 = NULL;
	h_ofc8 = (float *)malloc(ofc8_width * ofc8_height * sizeof(float));
	memset(h_ofc8,0,ofc8_width * ofc8_height * sizeof(float));
	
	fclayer<<<10000,1024>>>(d_ofc8, ofc8_width,ofc8_height, ofc8_depth, d_fc8,ifc8_width,ifc8_height,ifc8_depth,d_mconvfc8,opfilters,d_bconvfc8);
	
	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }
    
    cudaThreadSynchronize();
	cudaDeviceSynchronize();
	
	memcopy((void *)h_ofc8,d_ofc8,ofc8_width * ofc8_height * sizeof(float),cudaMemcpyDeviceToHost," host ofc8");
	//cprint(h_ofc8,ofc8_width * ofc8_height,ofc8_width,"Output from FC8",opfilters);
	
	//Free Layer16 resources
	deallocate(d_fc8, "Convfc8 input");
	deallocate(d_mconvfc8,"Convfc8 mask ");
	deallocate(d_bconvfc8,"Convfc8 bias ");
	free(convfc8);
	free(biasfc8);
	
	/*-----     */
	/* softmax  */
	softmax(h_ofc8,1000,LOG);
	/*-----     */
	/* Image classification */
	unsigned int classind = findMaxVal(h_ofc8,1000);
	std::string result = getStringByIndex("classes.txt",classind);
	printf("The image is %s\n",result.c_str());
	/*-----------------------*/
	
#endif
}

string getStringByIndex(string filename, unsigned int index)
{
	int count = 0;
	ifstream in(filename);	
 	string line;
	string retStr = " ";

	while(getline(in, line))
	{
		if(count == index)
		{
			retStr = line;
			break;
		}
		count++;
	}
	return retStr;
}


void softmax(float *input,unsigned int size, SoftmaxFn type)
{
    float sum = 0;
    if(type == EXPO)
    {
        for(unsigned int i = 0;i<size;i++)
        {
            sum+= (float)exp(input[i]);
        }

        for(unsigned int i = 0;i<size;i++)
        {
            input[i] =((float) exp(input[i]) )/ sum ;
        }
    }
    else if(type == LOG)
    {
        for(unsigned int i = 0; i < size; i++)
        {
            input[i] = log(input[i]);
        }
    }

}

unsigned int findMaxVal(float *input,unsigned int size)
{
    unsigned int maxind = 0;
    for(unsigned int i = 1;i<size;i++)
    {
        if(input[maxind] < input[i])
        {
            maxind = i;
        }
    }
    
    return maxind;
}


void read_fc_file(float arr1[], int arr1_size, float arr2[], int arr2_size, const char* fileName)
{
	char* line = NULL;
	char* ch = NULL;

	FILE* fp;
	size_t len = 0;
	ssize_t read;
	int lineCount = 1; //1 to fileNumber
	int count = 0;
	fp = fopen(fileName, "r");
	bool secondArray = false;
	
	int secondArrayCount = 0;
	int firstArrayIndex = 0;
	int secondArrayIndex = 0;
	
	if(fp == NULL)
	{
		printf("NULL");
		exit(EXIT_FAILURE);
	}
	else
	{
		//printf("file can be parsed\n");
		while ((read = getline(&line, &len, fp)) != -1)
		{
        	float ftemp = atof(line);
        	ch = strchr(line, '"');
        	if( (!secondArray) && (strchr(line, '"') != NULL) )
			{
				count++;
				if(count == 4)
				{
					secondArray = true;
					//printf("Before Bias:%d\n", lineCount);
					continue;
				}
				else
				{
					continue;
				}
			}

        	if( (!secondArray) && (!count))
			{
				float temp_1 = atof(line);
				if(firstArrayIndex < arr1_size)
				{
					arr1[firstArrayIndex] = temp_1;
				}
				firstArrayIndex++;
			}
			else if(secondArray)
			{
				char* ch = strtok(line, " ");
				while(ch != NULL)
				{
					float temp_2 = atof(ch);
					if(secondArrayIndex < arr2_size)
					{
						arr2[secondArrayIndex] = temp_2;
						secondArrayIndex++;
					}
					ch = strtok(NULL, " ");
				}
			}
        	lineCount++;
    	}
	}
	//printf("lineCount: %d\nfirstArrayIndex: %d\nsecondArrayIndex: %d\ncount: %d\n", lineCount, firstArrayIndex, secondArrayIndex, count);
}

void read_two_file(float arr1[], unsigned int arr1_size, float arr2[], unsigned int arr2_size, string fileName)
{
	ifstream in(fileName);
	string line;
	int count = 0;
	int secondArrayCount = 0;
	unsigned int firstArrayIndex = 0;
	int secondArrayIndex = 0;
	int totalCount = 0;
	bool secondArray = false;
	static float temp_1, temp_2;

	while(in >> line)
	{
		if( (!secondArray) && (line.find('"') != string::npos))
		{
			count++;
			if(count == 2)
			{
				secondArray = true;
				continue;
			}
		}
		if( (!secondArray) && (!count))
		{
			float temp_1;
			temp_1 = ::atof(line.c_str());
//			cout << temp_1;
			if(firstArrayIndex < arr1_size)
			{
				arr1[firstArrayIndex] = temp_1;
			}
			firstArrayIndex++;
		}
		else if(secondArray)
		{
			float temp_2;			
			temp_2 = ::atof(line.c_str());
			//cout << temp_2;
			if(secondArrayIndex < arr2_size)
			{
				arr2[secondArrayIndex] = temp_2;
			}			
			secondArrayIndex++;		
		}
		totalCount++;
	} 
	cout << firstArrayIndex << "\n2nd: " << secondArrayIndex << " \ntotal" << totalCount << "\ncnt: " << count << "\n";
	//return 0;
}


void demo()
{

	cudaError_t err = cudaSuccess;
		
	/* Convolution test */
	unsigned int mask_width = 3; //columns
	unsigned int mask_height = 18; //rows
	unsigned int mask_size =sizeof(float)*mask_width * mask_height;
	unsigned int width = 5; //columns
	unsigned int height = 15; //rows
	unsigned int size=sizeof(float)*width * height;
	
	
	float *h_output= ( float *)malloc( size);
	float *h_input = ( float *)malloc( size);
	float *h_mask = ( float *)malloc(mask_size);
	float *h_bias = (float *)malloc(size);
	memset(h_bias,0,size);

	for(int i=0;i<width*height;i++)
	{

		h_input[i] = (float) i;

	}

	for(int i=0;i<mask_width*mask_height;i++)
	{

		h_mask[i] = (float) i;

	}

	memset(h_output,0,size);
	
	printf("Convolution input");	
	for(int i=0;i<width*height;i++)
	{
		printf((i%width) == 0 ?"\n":" ");
		printf("%2.2f",h_input[i]);
	}
	printf("\n");


	printf("Convolution mask");	
	for(int i=0;i<mask_width*mask_height;i++)
	{
		printf((i%mask_width) == 0 ?"\n":" ");
		printf("%0.3f",h_mask[i]);
	}
	printf("\n");

	
	float *d_input = NULL;
	err = cudaMalloc((void**)&d_input,width*height*sizeof(float));
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate device input (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	float *d_output = NULL;
	err = cudaMalloc((void**)&d_output,width*height*sizeof(float));
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate device output (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	float *d_mask = NULL;
	err = cudaMalloc((void**)&d_mask,mask_width*mask_height*sizeof(float));
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate device mask (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}

	float *d_bias = NULL;
	err = cudaMalloc((void**)&d_bias,size);
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate device mask (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}


	err = cudaMemcpy(d_input,h_input,width*height*sizeof(float),cudaMemcpyHostToDevice);
	if(err!= cudaSuccess)
	{
		fprintf(stderr,"Failed to copy from host to device (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
		
	err = cudaMemcpy(d_mask,h_mask,mask_width*mask_height*sizeof(float),cudaMemcpyHostToDevice);
	if(err!= cudaSuccess)
	{
		fprintf(stderr,"Failed to copy from host to device (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	
	err = cudaMemcpy(d_bias,h_bias,size,cudaMemcpyHostToDevice);
	if(err!= cudaSuccess)
	{
		fprintf(stderr,"Failed to copy from host to device (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}

	
	//execFirstLayer(d_output,d_input,d_mask);
	
	
	conv1d<<<65536,512>>>(d_output,d_input,5,15,d_mask,3,9,3,3,d_bias);
	//conv1d<<<65536,512>>>(output,input,5,5,mask,3,3,1);
	//add input and filter count to conv function


	err = cudaGetLastError();
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to launch kernel (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    cudaDeviceSynchronize();

	err = cudaMemcpy(h_output,d_output,width*height*sizeof(float),cudaMemcpyDeviceToHost);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy output from device to host (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	printf("Convolution result\n");
	for(int i=0;i<width*height;i++)
	{
		printf((i%width) == 0 ?"\n":" ");
		printf("%2.3f",h_output[i]);
		
	}
	printf("\n");
	
	cudaFree(d_input);
	cudaFree(d_output);
	cudaFree(d_mask);
	
	free(h_input);
	free(h_output);
	free(h_mask);	
	
	/* maxpool test*/	
	unsigned int maxin_width = 8;//4; //columns
	unsigned int maxin_height = 8;//8;//rows
	unsigned int filters = 2;
	unsigned int filter_width = 2;
	unsigned int filter_height = 2;
	unsigned int stride = 2;
	unsigned int maxop_width =  (((maxin_width - filter_width) / stride) + 1); //columns
	unsigned int maxop_height = (((maxin_height - filter_width) / stride) + 1); //rows
	
	float *maxin = (float *) malloc(sizeof(float) * maxin_width * maxin_height);
	
	float *maxop = (float *) malloc(sizeof(float) * maxop_width * maxop_height); 
	memset(maxop,0,sizeof(float) * maxop_width * maxop_height);
	

	
	float *d_maxin = NULL;
	err = cudaMalloc((void**)&d_maxin,maxin_width*maxin_height*sizeof(float));
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate maxpool input (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	float *d_maxop = NULL;
	err = cudaMalloc((void**)&d_maxop,maxop_width * maxop_height * sizeof(float));
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate maxpool output (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	printf(" \nmax pool input");
	for(int i = 0 ; i<(maxin_width * maxin_height); i++)
	{
		maxin[i] = float(i);
		printf( (i % maxin_width) == 0 ? "\n":" ");
		printf("%0.2f",maxin[i]);
	}
	printf("\n");
	
	err = cudaMemcpy(d_maxin,maxin,maxin_width*maxin_height*sizeof(float)/*size*/,cudaMemcpyHostToDevice);
	if(err!= cudaSuccess)
	{
		fprintf(stderr,"Failed to copy from host to device (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	maxpool<<<65536,1024>>>(d_maxop, maxop_width,maxop_height,d_maxin, maxin_width, maxin_height, filters, stride, filter_width,filter_height);
	
	err = cudaMemcpy(maxop,d_maxop,maxop_width*maxop_height*sizeof(float),cudaMemcpyDeviceToHost);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy output from device to host (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	printf("\nmaxpool output"); 
	for(int i =0;i<maxop_width*maxop_height;i++)
	{
		printf( (i % maxop_width) == 0 ? "\n":" ");
		printf("%0.2f",maxop[i]);
		
	}
	printf("\n");
	cudaFree(d_maxin);
	cudaFree(d_maxop);
	/*------------*/
	
	/* FC test */
	unsigned int fcin_width = 3;
	unsigned int fcin_height = 3;
	unsigned int fcin_depth = 3;
	
	unsigned int fcmask_width = fcin_width ;
	unsigned int fcmask_height = fcin_height ;
	unsigned int fcfilters = fcin_depth * 3;
	
	unsigned int fcout_width = 1;
	unsigned int fcout_height = 1;
	unsigned int fcout_depth = fcfilters/fcin_depth;

	float *fcin = NULL;
	fcin = (float *) malloc (sizeof(float) * fcin_width * fcin_height * fcin_depth);
	
	float *fcmask = NULL;	
	fcmask = (float *) malloc (sizeof(float) * fcmask_width * fcmask_height * fcfilters);
	
	float *fcbias = NULL;
	fcbias = (float *)malloc(sizeof(float)*fcout_depth);
	
	float *fcout = NULL;
	fcout = (float *) malloc (sizeof(float) * fcout_width * fcout_height * fcout_depth);
	
	printf("\n FC input");
	for(int i=0;i<fcin_width*fcin_height*fcin_depth;i++)
	{
		fcin[i] = (float) i;
		printf( (i % fcin_width) == 0 ? "\n":" ");
		printf("%0.2f",fcin[i]);
	}
	
	printf("\n FC mask");
	for(int i=0;i<fcmask_width*fcmask_height*fcfilters;i++)
	{
		fcmask[i] = (float) i;
		printf( (i % fcmask_width) == 0 ? "\n":" ");
		printf("%0.2f",fcmask[i]);
	}
	
	printf("\n FC Bias");
	for(int i=0;i<fcout_depth;i++)
	{
		fcbias[i] = (float) i;
		printf( (i % fcout_width) == 0 ? "\n":" ");
		printf("%0.2f",fcbias[i]);	
	}

	float *d_fcin = NULL;
	err = cudaMalloc((void**)&d_fcin,sizeof(float) * fcin_width * fcin_height * fcin_depth);
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate FC input (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	float *d_fcmask = NULL;
	err = cudaMalloc((void**)&d_fcmask,sizeof(float) * fcmask_width * fcmask_height * fcfilters);
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate FC mask (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	float *d_fcbias = NULL;
	err = cudaMalloc((void**)&d_fcbias,sizeof(float) * fcout_depth);
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate FC mask (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	float *d_fcout = NULL;
	err = cudaMalloc((void**)&d_fcout,sizeof(float) * fcout_width * fcout_height * fcout_depth);
	if(err!=cudaSuccess)
	{
		fprintf(stderr,"Failed to allocate FC output (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	err = cudaMemcpy(d_fcin,fcin,fcin_width*fcin_height*fcin_depth*sizeof(float),cudaMemcpyHostToDevice);
	if(err!= cudaSuccess)
	{
		fprintf(stderr,"Failed to copy from host to device (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}
	
	err = cudaMemcpy(d_fcmask,fcmask,fcmask_width*fcmask_height*fcfilters*sizeof(float),cudaMemcpyHostToDevice);
	if(err!= cudaSuccess)
	{
		fprintf(stderr,"Failed to copy from host to device (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}

	err = cudaMemcpy(d_fcbias,fcbias,fcout_depth*sizeof(float),cudaMemcpyHostToDevice);
	if(err!= cudaSuccess)
	{
		fprintf(stderr,"Failed to copy from host to device (error code %s)!\n",cudaGetErrorString(err));	
		exit(EXIT_FAILURE);
	}

	//kernel call
	fclayer<<<65536,1024>>>(d_fcout,  fcout_width,fcout_height, fcout_depth, d_fcin,fcin_width,fcin_height,fcin_depth,d_fcmask,fcfilters,d_fcbias);
	
	err = cudaMemcpy(fcout,d_fcout,fcout_width*fcout_height*fcout_depth*sizeof(float),cudaMemcpyDeviceToHost);
    if (err != cudaSuccess)
    {
        fprintf(stderr, "Failed to copy output from device to host (error code %s)!\n", cudaGetErrorString(err));
        exit(EXIT_FAILURE);
    }

	printf("\nFC output"); 
	for(int i =0;i<fcout_width*fcout_height*fcout_depth;i++)
	{
		printf( (i % fcout_width) == 0 ? "\n":" ");
		printf("%0.2f",fcout[i]);
		
	}
	printf("\n");
	
	cudaFree(d_fcin);
	cudaFree(d_fcmask);
	cudaFree(d_fcout);

	/*------------*/
	
	
	
	
	/* 		  */
	
	
}

