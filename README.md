#			VGGNet 16 implementation in CUDA

**Folder hierarchy**

The main directory also contains many python scripts that are used to take image as an input, resize it to 224x224 pixels and extracts RGB values into 3 arrays that are stored in header files. There are scripts that were part of the initial approach where weight values were parsed and statically written into header files. But this approach was changed due to the compilation time/errors we came across. Now the network will read weights dynamically during runtime.
weights directory -  this directory contains the weight text files that are read and parsed when the neural network executes.

**Build**

Make the build script executable using
```
$ sudo chmod +x build.sh
```
Always invoke build.sh with the input image file as the argument when you want to execute the network.
for eg ./build.sh image.jpg
to build and execute the network wth image.jpg as the input image

**Code**

The code provides different debug levels that can be used by uncommenting DEBUG_PRINT/ DEBUG_PRINT_2/ DEBUG_MAXPOOL in VGGNet_GPU.cu
ELEMEMT and ELEMENTM can be set to check the value of that element in the output of Convolution and Maxpool layers respectively
Additionally DEMO can be uncommented in VGGNet_GPU to check the functionality of the network with smaller custom inputs.
