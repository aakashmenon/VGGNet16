import sys
import fileinput


"""
Input : filename.py inputfile.txt headerfile.h array1 array2

"""
count = 0
bias = 0
flag=0
last_pos=0

input_file = sys.argv[1]
output_file = sys.argv[2] 
weight_array = sys.argv[3]

print(input_file)
print(output_file)

number = []
number_bias = []

my_file = open(input_file, "r") 

header_file = open(output_file,"w+")

for line in my_file:
	number.append(line)

print('\n')

weight_string = ""

for i in range(len(number)):
	if (i==len(number)-1):
		weight_string = weight_string + str(number[i])
	elif(i<len(number)):
		weight_string = weight_string + str(number[i]) + ','

weight_string = weight_string.replace(' ', ',\n')

header_file.write('float '+ weight_array + '[] = {' + weight_string +  '};\n\n')

header_file.close()

my_file.close()
	

