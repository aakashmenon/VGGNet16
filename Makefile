.SUFFIXES:  .cpp .cu .o
CUDA_HOME := /usr/local/cuda
INC	:= -I$(CUDA_HOME)/include -I.
LIB	:= -L$(CUDA_HOME)/lib 
CC	:= nvcc
OBJS := VGGNet_CPU.o VGGNet_GPU.o
DEP	:= layers.h 

NVCCFLAGS	:= -lineinfo -arch=sm_53 -std=c++11 #--ptxas-options=-v -g 

all: vggnet

vggnet:	$(OBJS) $(DEP)
	$(CC) $(INC) $(NVCCFLAGS) -o vggnet $(OBJS) $(LIB)

.cpp.o:
	$(CC) $(INC) $(NVCCFLAGS) -c $< -o $@ 

.cu.o:
	$(CC) $(INC) $(NVCCFLAGS) -c $< -o $@
	

clean:
	rm -f *.o vggnet


