#include <iostream>
#include <stdint.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include "layers.h"
/*
using namespace std;

void read_two_file(float arr1[], int arr1_size, float arr2[], int arr2_size, string fileName)
{
	ifstream in(fileName);
	string line;
	int count = 0;
	int secondArrayCount = 0;
	unsigned int firstArrayIndex = 0;
	int secondArrayIndex = 0;
	int totalCount = 0;
	bool secondArray = false;
	static float temp_1, temp_2;

	while(in >> line)
	{
		if( (!secondArray) && (line.find('"') != string::npos))
		{
			count++;
			if(count == 2)
			{
				secondArray = true;
				continue;
			}
		}
		if( (!secondArray) && (!count))
		{
			float temp_1;
			temp_1 = ::atof(line.c_str());
//			cout << temp_1;
			if(firstArrayIndex < arr1_size)
			{
				arr1[firstArrayIndex] = temp_1;
			}
			firstArrayIndex++;
		}
		else if(secondArray)
		{
			float temp_2;			
			temp_2 = ::atof(line.c_str());
			//cout << temp_2;
			if(secondArrayIndex < arr2_size)
			{
				arr2[secondArrayIndex] = temp_2;
			}			
			secondArrayIndex++;		
		}
		totalCount++;
	} 
	cout << firstArrayIndex << "\n2nd: " << secondArrayIndex << " \ntotal" << totalCount << "\ncnt: " << count << "\n";
	//return 0;
}

*/

/*

Vggnet 16 in a nutshell

data->conv->relu->pool->conv->relu->pool->FC->softmax

(16 layers in total)

*/

/*
weight row majored
R G B
*/


/*
class Matrix
{
public:
	__device__ __host__ Matrix();
	__device__ __host__ Matrix(size_t rows,size_t columns); //edit here
	__device__ __host__ ~Matrix();
	__device__ __host__ getWidth();
	__device__ __host__ getHeight();
	__device__ __host__ getSize();
	__device__ __host__ getvalue(int row,int column);
	__device__ __host__ setvalue(int row,int column,int val);
private:
	int width,height,size;
	int *elements;
};

int Matrix::getWidth()
{
	return width;
}

int Matrix::getHeight()
{
	return height;
}

int Matrix::getSize()
{
	return size;
}

Matrix::Matrix() : width(0),height(0),size(0),elements(nullptr)
{
}

Matrix::Matrix(size_t row, size_t columns)
{
	elements = new int[row*columns];
	size = row * columns;
	width = columns;
	height = row;
}

Matrix::~Matrix()
{
	delete[] elements;
}

void Matrix::print()
{
	if(0 == width || 0 == height)
	{
		printf("ERROR!!! Need to assign rows and columns\n");
		return;
	}

	for(int i=0;i<width;i++)
	{
		for(int j=0;j<height;j++)
		{
			printf("%3d ",getvalue(i,j));
		}
		printf("\n");
	}
}

void Matrix::setvalue(int row,int column, int val)
{
	if(0 == width || 0 == height)
	{
		printf("ERROR!!! Need to assign rows and columns\n");
		return;
	}
	elements[row * width + column] = val;
}

int Matrix::getvalue(int row,int column)
{
	return elements[row*width + column];
}

int Matrix::getvalue(int pos)
{
	return elements[pos];
}

Matrix conv(Matrix& input,Matrix& mask)
{
	int product=0;
	Matrix result(input.getWidth(),input.getHeight());
	for(int i=0;i<input.getWidth();i++)
	{
		for(int j=0;j<input.getHeight();j++)
		{
			for(int k=0;k<mask.getWidth();k++)
			{
				for(int l=0;l<mask.getHeight();l++)
				{

					int tempi = i-(mask.getWidth()/2) + k;
					int tempj = j-(mask.getHeight()/2) + l;


					if(tempi >= 0 && tempi < input.getWidth() && tempj >= 0 && tempj <input.getHeight())
					{
						printf("input[%d][%d]=%d * mask[%d][%d]=%d \n",tempi,tempj,input.getvalue(tempi,tempj),k,l,mask.getvalue(k,l));

					product+= mask.getvalue(k,l) * input.getvalue(tempi,tempj); 
						
					}



				}	
			}
			result.setvalue(i,j,product);
			product=0;
			printf("\n\n");
		}
	}
return result;
}

*/



int main(int argc, char *const argv[])
{
	//read image jpeg format
	
	//In the paper, the model is denoted as the configuration D trained with scale jittering.
	// The input images should be zero-centered by mean pixel (rather than mean image) subtraction. 
	//Namely, the following BGR values should be subtracted: [103.939, 116.779, 123.68]

	//read weights and biases- convert from string to float
	//copy from cpu to gpu
	// in gpu
	// execute all layers
	//get result
	//display result

	float *input, *output, *mask;

	execFirstLayer(output,input,mask);
	
	return 0;
}

//input resolution 224 x 224 x 3?
//horizontal and vertical stride
//bias per element or per kernel?
//pad with 0 or boundary element?
//data set

//need to list each layer and weight kernel dimensions
//outputs not between 0 and 1?	
// input weights row major/column major

/*
convolution


	for(depth)
		for(filter size)
	+bias
	relu

*/
