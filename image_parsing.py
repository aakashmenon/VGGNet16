from PIL import Image, ImageFilter
from resizeimage import resizeimage
import sys

debug = 0
WIDTH = 224
HEIGHT = 224


input_file = sys.argv[1]
resized = "resized_"+input_file

if debug:
	print(input_file)

with open(input_file, 'r+b') as f:
	with Image.open(f) as image:
		width,height = image.size
		if width is not WIDTH or height is not HEIGHT:
			cover = resizeimage.resize_cover(image, [WIDTH, HEIGHT])
			cover.save(resized, image.format)
			resized_name = 1
		else:
			resized_name = 0
#Read image
if resized_name:
	im = Image.open( resized )
else:
	im = Image.open( input_file )	
#Display image
if debug:
	im.show()
im.show()
#Splitting the image into its respective bands, i.e. Red, Green,
#and Blue for RGB
r,g,b = im.split()

if debug:
	r.show(r)
	g.show(g)
	b.show(b)

#r.save('red.png','PNG')

#g.save('green.png','PNG')

#b.save('blue.png','PNG')

r_data = list(r.getdata())

g_data = list(g.getdata())

b_data = list(b.getdata())

#print(r_data)
if debug:
	print(len(r_data))
	print(len(g_data))
	print(len(b_data))

r_string = ""
b_string = ""
g_string = ""

for item in range(len(r_data)):
	if (item == len(r_data)-1):
		r_string = r_string + str(r_data[item] )
	elif(item < len(r_data)):
		r_string = r_string + str(r_data[item] - 123.68) + ",\n"


for item in b_data:
	if (item == len(r_data)-1):
		b_string = b_string + str(b_data[item])
	elif(item < len(r_data)):
		b_string = b_string + str(b_data[item] - 116.779) + ",\n"



for item in g_data:
	if (item == len(g_data)-1):
		g_string = g_string + str(g_data[item])
	elif(item < len(g_data)):
		g_string = g_string + str(g_data[item] - 103.939) + ",\n"

R_data_file = open('Red_data.h',"w+")
R_data_file.write('#ifndef __RED_DATA_H__\n#define __RED_DATA_H__\nstatic const float red_array[] = {' + r_string + '}; \n#endif' )
R_data_file.close()


B_data_file = open('Blue_data.h',"w+")
B_data_file.write('#ifndef __BLUE_DATA_H__\n#define __BLUE_DATA_H__\nstatic const float blue_array[] = {' + b_string + '}; \n#endif' )
B_data_file.close()



G_data_file = open('Green_data.h',"w+")
G_data_file.write('#ifndef __GREEN_DATA_H__\n#define __GREEN_DATA_H__\nstatic const float green_array[] = {' + g_string +'};\n#endif ')
G_data_file.close()
#print(r_data)

#print(g_data)

#print(b_data)




"""
from PIL import Image, ImageFilter
#Read image
im = Image.open( 'lena.png' )
#Display image
im.show()

#Applying a filter to the image
im_sharp = im.filter( ImageFilter.SHARPEN )
#Saving the filtered image to a new file
im_sharp.save( 'image_sharpened.jpg', 'JPEG' )

#Splitting the image into its respective bands, i.e. Red, Green,
#and Blue for RGB
r,g,b = im_sharp.split()

#Viewing EXIF data embedded in image
exif_data = im._getexif()
exif_data
"""
