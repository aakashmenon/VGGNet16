#ifndef __LAYERS_H__
#define __LAYERS_H__

#include <iostream>
#include <stdint.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda.h>

#include <stdio.h>
#include <algorithm>
#include <string.h>
#include <fstream>

#include "Red_data.h"
#include "Green_data.h"
#include "Blue_data.h"
#include "weights/conv11.h"
#include "weights/conv12.h"
#include "weights/conv21.h"
#include "weights/conv22.h"
/*#include "weights/conv31.h"
#include "weights/conv32.h"
#include "weights/conv33.h"
#include "weights/conv41.h"
#include "weights/conv42.h"
#include "weights/conv43.h"
#include "weights/conv51.h"
#include "weights/conv52.h"
#include "weights/conv53.h"
//#include "weights/fc6.h"
//#include "weights/fc7.h"
//#include "weights/fc8.h"
*/

enum SoftmaxFn{
  EXPO,
  LOG,
};



void execFirstLayer(float *output,float *input,float *mask);
void demo();
void read_two_file(float arr1[], unsigned int arr1_size, float arr2[], unsigned int arr2_size, std::string fileName);
void read_fc_file(float arr1[], int arr1_size, float arr2[], int arr2_size, const char* fileName);
void softmax(float *input,unsigned int size, SoftmaxFn type);
unsigned int findMaxVal(float *input,unsigned int size);
std::string getStringByIndex(std::string filename, unsigned int index);

#endif
