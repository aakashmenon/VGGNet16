import sys
import fileinput

debug = 0

"""
Input : filename.py inputfile.txt headerfile.h array1 array2

"""
count = 0
bias = 0
flag=0
last_pos=0

input_file = sys.argv[1]
output_file = sys.argv[2] 
weight_array = sys.argv[3]
bias_array = sys.argv[4] 

if debug:
	print(input_file)
	print(output_file)

number = []
number_bias = []

my_file = open(input_file, "r") 

header_file = open(output_file,"w+")


for line in my_file:
	if "\"" in line:
		count += 1
	elif count == 2 and "\"" not in line:
		number_bias.append(line.rstrip('\n'))
	elif count == 0:
		number.append(line)#.rstrip('\n')

if debug:
	print(number)
	print('\n')
	print(number_bias)


weight_string = ""

bias_string = ""

count =0
for i in range(len(number)):
	if (i==len(number)-1):
		weight_string = weight_string + str(number[i])
	elif(i<len(number)):
		weight_string = weight_string + str(number[i]) + ','
	count+=1;
	if(count%10000) is 0:
		print(count)

print("PARSING WEIGHTS DONE")

for i in range(len(number_bias)):
	if (i==len(number_bias)-1):
		bias_string = bias_string + str(number_bias[i])
	elif(i<len(number_bias)):
		bias_string = bias_string + str(number_bias[i])

bias_string = bias_string.replace(' ', ',\n')
print("PARSING BIASES DONE")


header_file.write('static const float '+ weight_array + '[] = {' + weight_string +  '};\n\n')

header_file.write('static const float '+ bias_array + '[] = {' + bias_string +  '};')

header_file.close()

my_file.close()
	

